---
title: Blog first post
date: 2017-10-06
---
# Voorbeeld post

## Done
Dit is een voorbeeld post voor de **workshop** bloggen
Wat we hebben gedaan is:
1. Gitlab aangemaakt
2. Hugo geforkt
3. Settings aangepast
4. Blogposts verwijderd
5. Een nieuwe post online gezet

---

## En verder
Wat we nog moeten doen:
* Thema wijzigen
* Nog meer settings aanpassen
* Meer bloggen
* _Iets wat cursief is_
* en nog meer!